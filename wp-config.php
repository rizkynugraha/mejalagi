<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'meja_mejamakan');

/** MySQL database username */
define('DB_USER', 'mejadb');

/** MySQL database password */
define('DB_PASSWORD', 'mejadb!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H/out4xn7v5ef_~<Dmy!lKFbP1B1y(F}BZ^idil7XhUgM0w{x]@SB:4,b{~M.;A,');
define('SECURE_AUTH_KEY',  '/bt}xq*YNTOGEmNHj)2liOlzgv?)Dc%E1mmpn4&M7?Y;|(l![|!?#J4)e)6rt~#U');
define('LOGGED_IN_KEY',    'eD8,a]uH2kq +@F0ZI(]wZ<y+O7!<S={X<#OcKVla/rH}iIIk&nIsB|mfRfQK0/,');
define('NONCE_KEY',        'x+9i[h3>$dv+Po+`keMKN`pZcecQn5T`q,lA#vjmH5`3v$^xu-t c<5b7CN7/k>;');
define('AUTH_SALT',        '-LF*Qz;21M#BsO_tplNYq.p;Oc73C/;uz$T,bI$r{Wm>Xb%X|ic1I!JWj+3HN8C-');
define('SECURE_AUTH_SALT', '<7Gy*E!c[yWFaylb^_y5yAJlH b]}F-RF1gv{)~75?2?E*}2wP6psfFF:fovkd>t');
define('LOGGED_IN_SALT',   'cjg&&nc}u6D-nH--BFEN]V!8$Z9I2R6[/(US)|w_od[V@mdx e8.<a!5i_]flU%N');
define('NONCE_SALT',       '/~Um{^aS;aPi%Qw]_Ht-V>8c><@J?a487$L]`$+uq.uZ$iv0x-/O)Of: Z[j@<-,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__));

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . '/wp-settings.php');