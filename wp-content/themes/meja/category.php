<?php get_header(); ?>

<div class="content">
	<div class="container">
		<h2 class="featured-title"><span><?php echo single_cat_title( '', false ); ?> </span></h2>
		<div class="row post-list">
			<?php  if ( have_posts() ) :
                    while (have_posts() ) : the_post(); ?>
						<div class="	col-md-4 posts">
							<div class="post-item">
								<div class="img-holder">
									<a href="<?php the_permalink(); ?>">
										<?php if(has_post_thumbnail()): ?>
		                                    <?php the_post_thumbnail('home-thumb',array('class'=>'img-responsive')); ?>
		                                <?php endif ;?>
									</a>
								</div>
								<div class="post-desc">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<a href="<?php the_permalink(); ?>" class="btn-warning btn">read more</a>
								</div>
							</div>
						</div>
                        <?php endwhile; ?>
                <?php wp_pagenavi( array( 'type' => 'multipart' ) ); ?>
           	<?php else: ?>
				<p>Oops sorry, sepertinya belum ada post dalam kategori ini</p>
           	<?php endif; ?>	
		</div>
	</div>
</div>
<?php get_footer(); ?>