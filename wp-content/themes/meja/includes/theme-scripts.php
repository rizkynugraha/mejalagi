<?php
/*-----------------------------------------------------------------------------------*/
/*	Register and load javascript
/*-----------------------------------------------------------------------------------*/
function cherry_scripts() {
	if (!is_admin()) {
		// CherryFramework Scripts
		wp_deregister_script('jquery');
		wp_register_script('jquery', CHILD_URL.'/assets/assets/jquery/dist/jquery.min.js', false, '2.1.1');
		wp_enqueue_script('jquery');
		
		wp_register_script('modernizr', PARENT_URL.'/js/modernizr.js', array('jquery'), '2.0.6');
		wp_register_script('matchHeight', CHILD_URL.'/assets/js/jquery.matchHeight-min.js', array('jquery'), '1.0');
		wp_register_script('swipe', CHILD_URL.'/assets/assets/jquery.event.swipe/js/jquery.event.swipe.js', array('jquery'), '1.0');
		wp_register_script('unslider', CHILD_URL.'/assets/assets/jquery.unslider/src/unslider.min.js', array('jquery'), '1.0');
		wp_register_script('owl', CHILD_URL.'/assets/assets/owl-carousel2/dist/owl.carousel.min.js', array('jquery'), '1.0');
		wp_register_script('custom', CHILD_URL.'/assets/js/custom.js', array('jquery'), '1.0');

		// Bootstrap Scripts
		wp_register_script('bootstrap', PARENT_URL.'/bootstrap/js/bootstrap.min.js', array('jquery'), '2.3.0');
		wp_enqueue_script('bootstrap');

		wp_enqueue_script('modernizr');
		wp_enqueue_script('matchHeight');
		wp_enqueue_script('swipe');
		wp_enqueue_script('unslider');
		wp_enqueue_script('owl');
		wp_enqueue_script('custom');

		
		
		
	}
}
add_action('wp_enqueue_scripts', 'cherry_scripts');

/*-----------------------------------------------------------------------------------*/
/*	Register and load stylesheet
/*-----------------------------------------------------------------------------------*/
function cherry_stylesheets() {
	
	// wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css', false, '3.2.1', 'all' );
	// wp_register_style( 'magnific-popup', PARENT_URL.'/css/magnific-popup.css', false, '0.9.3', 'all' );
	// wp_enqueue_style( 'magnific-popup' );
}
add_action('wp_enqueue_scripts', 'cherry_stylesheets');

/*-----------------------------------------------------------------------------------*/
/*	Register and load admin javascript
/*-----------------------------------------------------------------------------------*/
function tz_admin_js($hook) {
	$pages_array = array('post.php', 'post-new.php');
	if (in_array($hook, $pages_array)) {
		wp_register_script('tz-admin', PARENT_URL . '/js/jquery.custom.admin.js', 'jquery');
		wp_enqueue_script('tz-admin');
	}
}
add_action('admin_enqueue_scripts', 'tz_admin_js', 10, 1);
?>