<div class="footer">
	<div class="container">
		<div class="row widget-list">
			<?php get_sidebar(); ?>
		</div>
		<div class="copyright">
			&copy; 2014 Mejamakan
		</div>
	</div>
</div>
		
	<?php wp_footer(); ?> <!-- this is used by many Wordpress features and for plugins to work properly -->
</body>
</html>