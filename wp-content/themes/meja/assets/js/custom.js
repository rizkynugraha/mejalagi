$(function() {
    $('.banner').unslider({
    	delay: 3000,              //  The delay between slide animations (in milliseconds)
		// complete: function() {},  //  A function that gets called after every slide animation
		keys: true,               //  Enable keyboard (left, right) arrow shortcuts
		dots: true,               //  Display dot navigation
		fluid: true  
    });
    var width = $(window).width();
    // $('.dots').appendTo('.slider');
    if(width > 768){
    	$('.dropdown').hover(function() {
	         $(this).toggleClass('open');
	    });
    }
    $(window).resize(function(){
    	width = $(this).width();
    });
    $('.post-list').each(function(){
    	$(this).children('.posts').matchHeight(true);
    });

  	$('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1,
		            nav:true
		        },
		        600:{
		            items:3,
		            nav:true
		        },
		        1000:{
		            items:5,
		            nav:false,
		            loop:false
		        }
		    }
		});

     
});

