<?php get_header(); ?>
<div class="content">
		<div class="container">
			<div class="row post-detail">
				<?php while ( have_posts() ) : the_post(); ?>
				<div class="col-sm-12 post">
					<div class="post-item">
						<h1 class="featured-title">
							<span><?php the_title(); ?></span>
						</h1>
						<?php the_content(); ?>
					</div>
				</div>
				<?php endwhile; ?>
			</div>

		</div>
	</div>

<?php get_footer(); ?>