<?php get_header(); ?>
<div class="content">
		<div class="container">
			<div class="row post-detail">
				<?php while ( have_posts() ) : the_post(); ?>
				<div class="col-sm-9 post">
					<div class="post-item">
						<div class="recipe-heading">
							<div class="row">
								<div class="col-sm-5">
									<?php if(has_post_thumbnail()): ?>
			                            <?php the_post_thumbnail('home-thumb',array('class'=>'img-responsive')); ?>
			                        <?php endif ;?>	
								</div>
								<div class="col-sm-7">
									<h3>
										<?php the_title(); ?>
									</h3>
									<div class="desc">
										<ul>
											<li>Kategori Maknan <span><?php the_category(', '); ?></span></li>
											<li>
												Lokasi
												<span>
												<?php 
												global $post;
												$locations = wp_get_post_terms($post->ID, 'location');
												foreach($locations as $location):
											 	?>	
													<?= $location->name; ?>&nbsp;
											 	<?php endforeach; ?>
											 	</span>
										 	</li>
										 	<li>
												Bahan Dasar
												<span>
												<?php 
												global $post;
												$ingredients = wp_get_post_terms($post->ID, 'ingredients');
												foreach($ingredients as $ingredients):
											 	?>	
													<?= $ingredients->name; ?>&nbsp;
											 	<?php endforeach; ?>
											 	</span>
										 	</li>
										</ul> 
										
										
									</div>
								</div>
								
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="ingredients panel-default panel">
									<div class="panel-body">
										<h4><i class="fa fa-spoon"></i> Bahan Dasar</h4>
										<div class="ingredient-content">
											<?php the_field('ingredient'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="ingredients panel-default panel">
								<div class="panel-body">
									<h4><i class="fa fa-paste"></i> Cara Pengolahan</h4>
									<div class="ingredient-content">
										<?php the_field('instruction'); ?>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="comment">
						<?php comments_template(); ?>
						
					</div>
				</div>
				<div class="col-sm-3 sidebar">
					
						<div class="widget">
							<ul class="post-meta">
								<li class="post-date">
									Ditulis Pada:
									<span><?php the_time('d F Y') ?></span>
								</li>
								<li class="author">
									Profil Penulis:
									<div class="profile-img">
									<?php 
										// Retrieve The Post's Author ID
									    $user_id = get_the_author_meta('ID');
									    // Set the image size. Accepts all registered images sizes and array(int, int)
									    $size = 'home-thumb';

									    // Get the image URL using the author ID and image size params
									    $imgURL = get_cupp_meta($user_id, $size);

									    // Print the image on the page
									 ?>
										<img src="<?= $imgURL; ?>" alt="" class="img-responsive">
									</div>
									<h4><?php the_author_meta('nickname'); ?></h4>
									<p><?php the_author_meta('description'); ?></p>
								</li>
							</ul>
						</div>
					
				</div>
				<?php endwhile; ?>
			</div>

		</div>
	</div>

<?php get_footer(); ?>