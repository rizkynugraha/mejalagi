<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes();?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes();?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes();?>> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" <?php language_attributes();?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes();?>> <!--<![endif]-->
<head>
	<title><?php wp_title(); ?></title>
	<meta name="description" content="<?php wp_title(); echo ' | '; bloginfo( 'description' ); ?>" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="//gmpg.org/xfn/11" />
	<?php if(of_get_option('favicon') != ''){ ?>
	<link rel="icon" href="<?php echo of_get_option('favicon', '' ); ?>" type="image/x-icon" />
	<?php } else { ?>
	<link rel="icon" href="<?php echo CHILD_URL; ?>/favicon.ico" type="image/x-icon" />
	<?php } ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'atom_url' ); ?>" />
	<link rel="stylesheet" href="<?php echo CHILD_URL; ?>/assets/assets/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo CHILD_URL; ?>/assets/assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo CHILD_URL; ?>/assets/css/style.css">
	
	<?php
		/* Always have wp_head() just before the closing </head>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to add elements to <head> such
		 * as styles, scripts, and meta tags.
		 */
		wp_head();
	?>
	<?php
	/* The HTML5 Shim is required for older browsers, mainly older versions IE */ ?>
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
	</div>
	<![endif]-->
	
</head>

<body <?php body_class(); ?>>
	<div class="wood">
		<div class="container">
			<div class="row logo hidden-xs hidden-sm">
				<div class="col-md-4 col-md-push-4 text-center">
					<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt=""></a>
				</div>
			</div>
			<nav class="navbar navbar-default" role="navigation">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#"><img src="images/logo-nav.png" alt=""></a>
				</div>
			
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<?php 
						wp_nav_menu( array(
							'container'      => false,
							'menu_class'     => 'nav navbar-nav',
							'menu_id'        => 'topnav',
							'depth'          => 0,
							'theme_location' => 'header_menu',
							'walker'         => new wp_bootstrap_navwalker()
						));
					?>
				
					<form class="navbar-form navbar-right" role="search">
						<div class="form-group">
							<input type="text" class="form-control" name="s" placeholder="Search">
						</div>
						<button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
					</form>
					
				</div><!-- /.navbar-collapse -->
			</nav>
			<?php if(is_home()): ?>
				<div class="slider">
					<div class="banner">
					    <ul>
					    <!-- $query = new WP_Query(array('post_type'=>'post','category_name'=>'featured')); -->
					    <?php 
				                $query = new WP_Query(array('post_type'=>'post','category_name'=>'featured'));
				                if ( $query->have_posts() ) {
				                    while ( $query->have_posts() ) {
				                        $query->the_post();
				                        ?>
										<li>
								        	<?php 
								        		$image = get_field('slider_image'); 
								        		$imgURL = $image['sizes']['slider-post-thumbnail'];
								        		
								        	?>

								        	<img src="<?= $imgURL ?>" alt="">
								        	<div class="desc-holder">
								        		<div class="row">
								        			<div class="col-md-5 col-sm-12 desc">
								        				<h3><?php the_title(); ?></h3>
								        				<p><?php the_field('short_description'); ?></p>
								        				<a href="<?php the_permalink(); ?>" class="btn btn-warning">read more</a>
								        			</div>
								        		</div>
								        	</div>
								        </li>
										
				                        <?php
				                    }
				                }
				                wp_reset_postdata();
				            ?>	
					    
					    </ul>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	