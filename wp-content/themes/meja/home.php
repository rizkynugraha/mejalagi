<?php 
	get_header();
?>

<div class="content">
	<div class="container">
		<h2 class="featured-title"><span>Cita Rasa Kita</span></h2>
		<div class="row post-list">
			<?php 
                $query = new WP_Query(array('post_type'=>'post','posts_per_page'=>6));
                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        ?>
						<div class="	col-md-4 posts">
							<div class="post-item">
								<div class="img-holder">
									<a href="<?php the_permalink(); ?>">
										<?php if(has_post_thumbnail()): ?>
		                                    <?php the_post_thumbnail('home-thumb',array('class'=>'img-responsive')); ?>
		                                <?php endif ;?>
									</a>
								</div>
								<div class="post-desc">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<a href="<?php the_permalink(); ?>" class="btn-warning btn">read more</a>
								</div>
							</div>
						</div>
                        <?php
                    }
                }
                wp_reset_postdata();
            ?>	
		</div>
	</div>
</div>
<?php get_footer(); ?>